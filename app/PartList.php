<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartList extends Model
{
    protected $table = "partlist";
    protected $fillable= [
        'title', 'photo'
    ];
}
