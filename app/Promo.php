<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
    protected $table = "promo";
    protected $fillable= [
        'title', 'photo1', 'photo2', 'price', 'description'
    ];
}
