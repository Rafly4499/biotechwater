<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AboutBiotech extends Model
{
    protected $table = "about_biotech";
    protected $fillable= [
        'location', 'vission', 'mission', 'solve_problem', 'about', 'catalog', 'email', 'contact_person', 'no_hp', 'link_embed_map', 'link_video_profile', 'link_facebook', 'link_instagram'
    ];
}
