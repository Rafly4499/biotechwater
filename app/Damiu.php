<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Damiu extends Model
{
    protected $table = "damiu";
    protected $fillable= [
        'title', 'photo', 'price', 'description'
    ];
}
