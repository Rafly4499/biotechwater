<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Achievement extends Model
{
    protected $table = "achievement";
    protected $fillable= [
        'client', 'project', 'years_experience', 'support'
    ];
}
