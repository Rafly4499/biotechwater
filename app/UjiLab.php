<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UjiLab extends Model
{
    protected $table = "uji_lab";
    protected $fillable= [
        'title', 'photo'
    ];
}
