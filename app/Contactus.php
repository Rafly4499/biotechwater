<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contactus extends Model
{
    protected $table = "tb_contactus";
    protected $fillable= [
        'first_name', 'last_name', 'phone_number', 'subject', 'message', 'email',
    ];
}
