<?php

namespace App\Http\Controllers;

use App\Blog;
use App\CategoryBlog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Image;
use File;
class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog = Blog::all();
        return view('backend.pages.blog.index', compact('blog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Blog::orderBy('id')->get();
        $blog = Blog::all();
        $category = CategoryBlog::all();
        return view('backend.pages.blog.create', compact('blog','category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'title_blog' => 'required',
        //     'content_blog' => 'required',
        //     'photo_blog' => 'required',
        //     'tgl_posting' => 'required',
        //     'id_category_blog' => 'required',
        //     'id_user' => 'required',
        //     'slug' => 'required',
            
            
        // ]);

        $data = $request->all();
        $data['slug'] = Str::slug($request->get('title_blog'));
    	$tags = explode(",", $request->tags);
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $destinationPath = 'img/blog/'; // upload path
                $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111,99999).'.'.$extension; // renaming image
                $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
                Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                      $constraint->aspectRatio();
                      $constraint->upsize();
                  })->save($destinationPath.$fileName);
                $data['photo_blog'] = $fileName;
                unset($data['image']);
            }
          }

    	$blog = Blog::create($data);
        $blog->tag($tags);
        Session::flash('success', $data['title_blog'] . ' added successfully');
        return redirect('/panel/blog');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::find($id);
        $category = CategoryBlog::all();
        return view('backend.pages.blog.edit', compact('blog','category'));
    }
    public function view($id)
    {
        $blog = Blog::find($id);
        $category = CategoryBlog::all();
        return view('backend.pages.blog.view', compact('blog','category'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $req = $request->except('_method', '_token', 'submit');
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
              $destinationPath = 'img/blog/'; // upload path
            $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111,99999).'.'.$extension; // renaming image
            $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
            Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                  $constraint->aspectRatio();
                  $constraint->upsize();
              })->save($destinationPath.$fileName);
            $req['photo_blog'] = $fileName;
            unset($req['image']);
    
              $result = Blog::find($id);
              if (!empty($result->image)) {
                File::delete('img/blog/'.$result->image);
              }
            }else {
              unset($req['image']);
            }
          }else {
            unset($req['image']);
          }
          $tags = explode(",", $req['tags']);
          $req['slug'] = Str::slug($req['title_blog']);
          $data = Blog::where('id', $id)->update($req);
          $blog = Blog::where('id', $id)->firstorfail();
          $blog->tags = $request->tags;
          $blog->save();
          $blog->retag($tags);
	    Session::flash('success', $data['title_blog'] . ' updated successfully');
        return redirect('panel/blog');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Blog::find($id);
	    $data->destroy($id);

	    Session::flash('success', $data['title_blog'] . ' deleted successfully');
	    return redirect('panel/blog');
    }
}
