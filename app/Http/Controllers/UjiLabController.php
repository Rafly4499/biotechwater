<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UjiLab;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Image;
use File;
class UjiLabController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ujilab = UjiLab::all();
        return view('backend.pages.ujilab.index', compact('ujilab'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = UjiLab::orderBy('id')->get();
        $ujilab = UjiLab::all();
        return view('backend.pages.ujilab.create', compact('ujilab'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $destinationPath = 'img/ujilab/'; // upload path
                $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111,99999).'.'.$extension; // renaming image
                $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
                Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                      $constraint->aspectRatio();
                      $constraint->upsize();
                  })->save($destinationPath.$fileName);
                $data['photo'] = $fileName;
                unset($data['image']);
            }
          }

    	$ujilab = UjiLab::create($data);
        Session::flash('success', $data['title'] . ' added successfully');
        return redirect('/panel/ujilab');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = UjiLab::find($id);
        return view('backend.pages.ujilab.edit', ['ujilab' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $req = $request->except('_method', '_token', 'submit');
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
              $destinationPath = 'img/ujilab/'; // upload path
            $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111,99999).'.'.$extension; // renaming image
            $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
            Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                  $constraint->aspectRatio();
                  $constraint->upsize();
              })->save($destinationPath.$fileName);
            $req['photo'] = $fileName;
            unset($req['image']);
    
              $result = UjiLab::find($id);
              if (!empty($result->photo)) {
                File::delete('img/ujilab/'.$result->photo);
              }
            }else {
              unset($req['image']);
            }
          }else {
            unset($req['image']);
          }
          $data = UjiLab::where('id', $id)->update($req);
          
	    Session::flash('success', $data['title'] . ' updated successfully');
        return redirect('panel/ujilab');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = UjiLab::find($id);
	    $data->destroy($id);
	    Session::flash('success', $data['title'] . ' deleted successfully');
	    return redirect('/panel/ujilab');
    }
}
