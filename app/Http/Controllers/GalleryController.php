<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Image;
use File;
class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gallery = Gallery::all();
        return view('backend.pages.gallery.index', compact('gallery'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Gallery::orderBy('id')->get();
        $gallery = Gallery::all();
        return view('backend.pages.gallery.create', compact('gallery'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $destinationPath = 'img/gallery/'; // upload path
                $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111,99999).'.'.$extension; // renaming image
                $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
                Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                      $constraint->aspectRatio();
                      $constraint->upsize();
                  })->save($destinationPath.$fileName);
                $data['photo'] = $fileName;
                unset($data['image']);
            }
          }

    	$gallery = Gallery::create($data);
        Session::flash('success', $data['title'] . ' added successfully');
        return redirect('/panel/gallery');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Gallery::find($id);
        return view('backend.pages.gallery.edit', ['gallery' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $req = $request->except('_method', '_token', 'submit');
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
              $destinationPath = 'img/gallery/'; // upload path
            $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111,99999).'.'.$extension; // renaming image
            $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
            Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                  $constraint->aspectRatio();
                  $constraint->upsize();
              })->save($destinationPath.$fileName);
            $req['photo'] = $fileName;
            unset($req['image']);
    
              $result = Gallery::find($id);
              if (!empty($result->photo)) {
                File::delete('img/gallery/'.$result->photo);
              }
            }else {
              unset($req['image']);
            }
          }else {
            unset($req['image']);
          }
          $data = Gallery::where('id', $id)->update($req);
          
	    Session::flash('success', $data['title'] . ' updated successfully');
        return redirect('panel/gallery');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Gallery::find($id);
	    $data->destroy($id);
	    Session::flash('success', $data['title'] . ' deleted successfully');
	    return redirect('/panel/gallery');
    }
}
