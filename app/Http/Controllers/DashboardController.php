<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Portfolio;
use App\Blog;
use App\PartList;
use App\Gallery;
use App\Achievement;
class DashboardController extends Controller
{
    public function index()
    {
        $countportfolio = Portfolio::all()->count();
        $countblog = Blog::all()->count();
        $countpartlist = PartList::all()->count();
        $countgallery = Gallery::all()->count();
        $achieve = Achievement::first();
        return view('backend.pages.dashboard',compact('countportfolio','countpartlist', 'countblog', 'countgallery', 'achieve'));
    }
}
