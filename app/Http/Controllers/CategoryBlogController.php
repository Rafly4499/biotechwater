<?php

namespace App\Http\Controllers;

use App\CategoryBlog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
class CategoryBlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = CategoryBlog::all();
        return view('backend.pages.categoryblog.index', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = CategoryBlog::orderBy('id')->get();
        $category = CategoryBlog::all();
        return view('backend.pages.categoryblog.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['slug'] = Str::slug($request->get('name_category_blog'));
	    CategoryBlog::create($data);
	    Session::flash('message', $data['name_category_blog'] . ' added successfully');
	    return redirect('/panel/categoryblog');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CategoryBlog  $CategoryBlog
     * @return \Illuminate\Http\Response
     */
    public function show(CategoryBlog $CategoryBlog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CategoryBlog  $CategoryBlog
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = CategoryBlog::find($id);
        return view('backend.pages.categoryblog.edit', ['category' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CategoryBlog  $CategoryBlog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = CategoryBlog::find($id);
        $category = $request->all();
        $category['slug'] = Str::slug($category['name_category_blog']);
        $data->update($category);

	    Session::flash('success', $data['name_category_blog'] . ' updated successfully');
        return redirect('/panel/categoryblog');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CategoryBlog  $CategoryBlog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = CategoryBlog::find($id);
	    $data->destroy($id);

	    Session::flash('success', $data['name_category_blog'] . ' deleted successfully');
	    return redirect('/panel/categoryblog');
    }
}
