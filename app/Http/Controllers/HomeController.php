<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Portfolio;
use App\Blog;
use App\PartList;
use App\Gallery;
use App\Achievement;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('frontend.app');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
     public function adminHome()
     {
        $countportfolio = Portfolio::all()->count();
        $countblog = Blog::all()->count();
        $countpartlist = PartList::all()->count();
        $countgallery = Gallery::all()->count();
        $achieve = Achievement::first();
        return view('backend.pages.dashboard',compact('countportfolio','countpartlist', 'countblog', 'countgallery', 'achieve'));
     }
}
