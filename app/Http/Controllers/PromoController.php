<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Promo;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Image;
use File;
class PromoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promo = Promo::all();
        return view('backend.pages.promo.index', compact('promo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Promo::orderBy('id')->get();
        $promo = Promo::all();
        return view('backend.pages.promo.create', compact('promo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if ($request->hasFile('image1')) {
            if ($request->file('image1')->isValid()) {
                $destinationPath = 'img/promo/'; // upload path
                $extension = $request->file('image1')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111,99999).'.'.$extension; // renaming image
                $request->file('image1')->move($destinationPath, $fileName); // uploading file to given path
                Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                      $constraint->aspectRatio();
                      $constraint->upsize();
                  })->save($destinationPath.$fileName);
                $data['photo1'] = $fileName;
                unset($data['image1']);
            }
          }
          if ($request->hasFile('image2')) {
            if ($request->file('image2')->isValid()) {
                $destinationPath = 'img/promo/'; // upload path
                $extension = $request->file('image2')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111,99999).'.'.$extension; // renaming image
                $request->file('image2')->move($destinationPath, $fileName); // uploading file to given path
                Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                      $constraint->aspectRatio();
                      $constraint->upsize();
                  })->save($destinationPath.$fileName);
                $data['photo2'] = $fileName;
                unset($data['image2']);
            }
          }
    	$promo = Promo::create($data);
        Session::flash('success', $data['title'] . ' added successfully');
        return redirect('/panel/promo');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Promo::find($id);
        return view('backend.pages.promo.edit', ['promo' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $req = $request->except('_method', '_token', 'submit');
        if ($request->hasFile('image1')) {
            if ($request->file('image1')->isValid()) {
              $destinationPath = 'img/promo/'; // upload path
            $extension = $request->file('image1')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111,99999).'.'.$extension; // renaming image
            $request->file('image1')->move($destinationPath, $fileName); // uploading file to given path
            Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                  $constraint->aspectRatio();
                  $constraint->upsize();
              })->save($destinationPath.$fileName);
            $req['photo1'] = $fileName;
            unset($req['image1']);
    
              $result = Promo::find($id);
              if (!empty($result->photo1)) {
                File::delete('img/'.$result->photo1);
              }
            }else {
              unset($req['image1']);
            }
          }else {
            unset($req['image1']);
          }
          if ($request->hasFile('image2')) {
            if ($request->file('image2')->isValid()) {
              $destinationPath = 'img/promo/'; // upload path
            $extension = $request->file('image2')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111,99999).'.'.$extension; // renaming image
            $request->file('image2')->move($destinationPath, $fileName); // uploading file to given path
            Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                  $constraint->aspectRatio();
                  $constraint->upsize();
              })->save($destinationPath.$fileName);
            $req['photo2'] = $fileName;
            unset($req['image2']);
    
              $result = Promo::find($id);
              if (!empty($result->photo2)) {
                File::delete('img/promo/'.$result->photo2);
              }
            }else {
              unset($req['image2']);
            }
          }else {
            unset($req['image2']);
          }
          $data = Promo::where('id', $id)->update($req);
          
	    Session::flash('success', $data['title'] . ' updated successfully');
        return redirect('panel/promo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Promo::find($id);
	    $data->destroy($id);
	    Session::flash('success', $data['title'] . ' deleted successfully');
	    return redirect('/panel/promo');
    }
}
