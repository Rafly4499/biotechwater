<?php

namespace App\Http\Controllers;

use App\AboutBiotech;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
class AboutBiotechController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = AboutBiotech::first();
        return view('backend.pages.aboutbiotech.index', compact('item'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = AboutBiotech::orderBy('id')->get();
        $about = AboutBiotech::all();
        return view('backend.pages.aboutbiotech.create', compact('about'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
	    AboutBiotech::create($data);
	    Session::flash('message', ' added successfully');
	    return redirect('/panel/about');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\aboutbiotech  $aboutbiotech
     * @return \Illuminate\Http\Response
     */
    public function show(aboutbiotech $aboutbiotech)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\aboutbiotech  $aboutbiotech
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = AboutBiotech::find($id);
        return view('backend.pages.aboutbiotech.edit', ['about' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\aboutbiotech  $aboutbiotech
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = AboutBiotech::find($id);
        $about = $request->all();
        $data->update($about);

	    Session::flash('success', ' updated successfully');
        return redirect('/panel/about');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\aboutbiotech  $aboutbiotech
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = AboutBiotech::find($id);
	    $data->destroy($id);

	    Session::flash('success', ' deleted successfully');
	    return redirect('/panel/about');
    }
}
