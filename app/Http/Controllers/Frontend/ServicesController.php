<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ServicesController extends Controller
{
    public function jualbeli(){
        return view('frontend.pages.layanan.jualbeli');
    }
    public function lelangtawar(){
        return view('frontend.pages.layanan.lelangtawar');
    }
    public function pemberdayaanmitra(){
        return view('frontend.pages.layanan.pemberdayaanmitra');
    }
}
