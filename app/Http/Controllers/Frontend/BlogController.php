<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Blog;
use App\CategoryBlog;
use DB;
class BlogController extends Controller
{
    public function index(){
        $blog = Blog::paginate(6);
        $lastblog = Blog::orderBy('id','desc')->take(5)->get();
        $category = CategoryBlog::all();
        return view('frontend.pages.blog.index',compact('blog','category','lastblog'));      
    }
    public function categoryBlog($slug){
                $lastblog = Blog::orderBy('id','desc')->take(5)->get();
                $category = CategoryBlog::where('slug', $slug)->first();
                $allcategory = CategoryBlog::all();
                $blog = Blog::where('id_category_blog','=',$category->id)->paginate(4);
                return view('frontend.pages.blog.categoryblog',compact('category','blog','lastblog','allcategory'));
                
            }
    public function detailBlog($slug){
                
        $category = CategoryBlog::all();
        $lastblog = Blog::orderBy('id','desc')->take(5)->get();
        $blog = Blog::where('slug', $slug)->first();

        // $councom = BlogComment::find()->where('id_blog', '=', $blog->id)->count();
        // $comments = BlogComment::find()
		// 			->where('id_blog', '=', $blog->id)
        //             ->andWhere('status','=',1)
		// 			->orderBy('id', 'DESC')
        //             ->all();
        // ]);
        return view('frontend.pages.blog.detailblog',compact('blog','category','lastblog'));
                        
    }
    public function searchBlog(Request $request)
	{
        // menangkap data pencarian
        $lastblog = Blog::orderBy('id','desc')->take(5)->get();
        $category = CategoryBlog::all();
		$search = $request->search;
		$blog = Blog::where('title_blog','like',"%".$search."%")->paginate();
    		
		return view('frontend.pages.blog.searchblog',compact('blog','category','lastblog','search'));
 
    }
    public function tagBlog($slug){
        if($slug){
            $tag = Blog::Where('tags', 'like', '%' . $slug . '%')->first();
            $blog = Blog::Where('tags', 'like', '%' . $slug . '%')->paginate(4);
            if(!$blog){
                abort(404);
            }
        }
        $lastblog = Blog::orderBy('id','desc')->take(5)->get();
        $allcategory = CategoryBlog::all();
        $category = CategoryBlog::all();
        return view('frontend.pages.blog.tagblog',compact('tag','blog','lastblog','allcategory','category'));
        
    }
    // public function createcomment(Request $request)
    // {
    // $blogcomment = BlogComment::create([
    //     'comment' =>  $request->comment,
    //     'author' => $request->author,
    //     'email' => $request->email,
    //     'status' => 0,
    //     'id_blog' => $blog->id,
    // public function search(Request $request)
    // {
    //     $searchResults = (new Search())
    //         ->registerModel(Article::class, 'title', 'slug')
    //         ->registerModel(CategoryArticle::class, 'name')
    //         ->perform($request->input('searchQ'));
        
    //     return view('frontend.pages.knowledgebase.search', compact('searchResults'));
    // }
}
