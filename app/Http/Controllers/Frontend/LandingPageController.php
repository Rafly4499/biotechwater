<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Blog;
use App\CategoryBlog;
use App\PartList;
use App\Portfolio;
use App\UjiLab;
use App\Promo;
use App\Gallery;
use App\CaseStudy;
use App\Damiu;
use App\Achievement;
use App\AboutBiotech;
class LandingPageController extends Controller
{
    public function index(){
        $blog = Blog::limit(3)->get();
        $category = CategoryBlog::all();
        $partlist = PartList::limit(9)->get();
        $portfolio = Portfolio::all();
        $ujilab = UjiLab::limit(4)->get();
        $promo = Promo::limit(2)->get();
        $gallery = Gallery::all();
        $casestudy = CaseStudy::all();
        $damiu = Damiu::limit(4)->get();
        $achieve = Achievement::first();
        $about = AboutBiotech::first();
        return view('frontend.pages.landingpage.index',compact(
            'blog',
            'category',
            'partlist',
            'portfolio',
            'ujilab',
            'promo',
            'gallery',
            'casestudy',
            'damiu',
            'achieve',
            'about'
        ));
    }
}
