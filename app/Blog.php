<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = "tb_blog";
    use \Conner\Tagging\Taggable;
    protected $fillable= [
        'title_blog', 'content_blog', 'photo_blog', 'tgl_posting', 'id_category_blog', 'id_user', 'slug', 'tags',
    ];
    public function categoryblog(){
        return $this->belongsTo('App\CategoryBlog','id_category_blog');
    }
    public function user(){
        return $this->belongsTo('App\User','id_user');
    }
}
