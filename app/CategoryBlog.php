<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryBlog extends Model
{
    protected $table = "tb_categoryblog";
    protected $fillable= [
        'name_category_blog', 'slug',
    ];
    public function blog(){
        return $this->hasMany('App\Blog','id_category_blog');
    }
}
